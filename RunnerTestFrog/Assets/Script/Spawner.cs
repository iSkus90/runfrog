﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : ObjectPool
{
    [SerializeField] private GameObject[] _enemyTemplates;
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private float _secondsBetweenSpawn;
    [SerializeField] private float _decreaseTime;
    [SerializeField] private float _minTime = 0.8f;
    private float _elepserTime = 0;

    private void Start()
    {
        Initialize(_enemyTemplates);
    }
    private void Update()
    {
        _elepserTime += Time.deltaTime;
        if (_elepserTime >= _secondsBetweenSpawn)
        {
            if(TryGetObject(out GameObject enemy))
            {
                _elepserTime = 0;
                int spawnPointNumber = Random.Range(0, _spawnPoints.Length);
                SetEnemy(enemy, _spawnPoints[spawnPointNumber].position);
            }
            
            if (_secondsBetweenSpawn > _minTime)
            {
                _secondsBetweenSpawn -= _decreaseTime;
            }
        }
    }

    private void SetEnemy(GameObject enemy, Vector3 spawnPoint)
    {
        enemy.SetActive(true);
        enemy.transform.position = spawnPoint;
    }

}
