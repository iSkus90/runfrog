﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public GameObject[] _sound;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        RebootingSound();
    }

    public void RebootingSound()
    {
        _sound = GameObject.FindGameObjectsWithTag("Sound");

        if (_sound.Length==2)
        {
            Destroy(gameObject);
        }
    }
}
