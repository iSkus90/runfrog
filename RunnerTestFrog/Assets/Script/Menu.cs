﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject _start;

    public static Menu instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Pause();
    }

    public void LoseGameMenu()
    {
        _start.SetActive(false);
        Resume();
    }

    public void ShowStartMenu()
    {
        _start.SetActive(true);
        Pause();
        SceneManager.LoadScene(0);
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

}
