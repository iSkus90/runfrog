﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public bool IsGrounded;
    public bool IsGroundedTop;
    public LayerMask WhatIsGround;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _jumpCount;
    [SerializeField] private float _jumpCountMax;
    [SerializeField] public AudioSource _audioSource;
    [SerializeField] public AudioClip _audioJump;

    private Rigidbody2D _rigidbody2D;
    private Collider2D _myCollider;


    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _myCollider = GetComponent<Collider2D>();
    }

    void Update()
    {
        IsGrounded = Physics2D.IsTouchingLayers(_myCollider, WhatIsGround);
    }

    private void FixedUpdate()
    {
        _rigidbody2D.velocity = new Vector2(_moveSpeed, _rigidbody2D.velocity.y);
    }

    public void Jump()
    {
        if (_jumpCount < _jumpCountMax)
        {
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, _jumpForce);
            _jumpCount++;
            _audioSource.PlayOneShot(_audioJump);
        }

        if (IsGroundedTop==true)
        {
            _rigidbody2D.gravityScale = +7;
            transform.localScale = new Vector3(-0.7f, 0.5f, 0.5f);
            _audioSource.PlayOneShot(_audioJump);
        }
    }
    public void JumpDown()
    {
        _rigidbody2D.velocity = new Vector2(-_rigidbody2D.velocity.x, _jumpForce);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            _jumpCount = 0;
        }

        if (collision.gameObject.tag == "GroundTop")
        {
            transform.localScale = new Vector3(-0.7f, -0.5f, 0.5f);
            _rigidbody2D.gravityScale = -1;
            IsGroundedTop = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Obstacle obstacle = collision.gameObject.GetComponent<Obstacle>();
        if (obstacle)
        {
            Menu.instance.ShowStartMenu();
        }
    }
}
